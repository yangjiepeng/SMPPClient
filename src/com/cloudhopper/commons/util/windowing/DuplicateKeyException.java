package com.cloudhopper.commons.util.windowing;


/**
 * 如果请求的key（标识）存在的异常
 */
public class DuplicateKeyException extends Exception {
    static final long serialVersionUID = 1L;
    
    public DuplicateKeyException(String msg) {
        super(msg);
    }
    
}
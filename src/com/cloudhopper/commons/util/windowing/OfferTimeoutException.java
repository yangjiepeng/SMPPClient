package com.cloudhopper.commons.util.windowing;


/**
 * 超过指定的时间
 */
public class OfferTimeoutException extends Exception {
    static final long serialVersionUID = 1L;
    
    public OfferTimeoutException(String msg) {
        super(msg);
    }
    
    public OfferTimeoutException(String msg, Throwable t) {
        super(msg, t);
    }
    
}
package com.cloudhopper.commons.util.windowing;


/**
 * 调用者等待超过指定时间
 */
public class PendingOfferAbortedException extends OfferTimeoutException {
    
    public PendingOfferAbortedException(String msg) {
        super(msg);
    }
    
    public PendingOfferAbortedException(String msg, Throwable t) {
        super(msg, t);
    }
    
}
package com.cloudhopper.commons.util.windowing;


/**
 * 发送窗口的事件监听器
 */
public interface WindowListener<K,R,P> {

    /**
     * 句柄等待、发送被超时的通知
     */
    public void expired(WindowFuture<K,R,P> future);

}

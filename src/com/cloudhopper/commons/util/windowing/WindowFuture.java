package com.cloudhopper.commons.util.windowing;


/**
 * 发送句柄有uncompleted、completed状态，发送请求开始，新句柄默认uncompleted
 * 如果它成功、失败、被取消都标记为完成
 * <pre>
 *                                      +---------------------------+
 *                                      | Completed successfully    |
 *                                      +---------------------------+
 *                                 +---->      isDone() = <b>true</b>      |
 * +--------------------------+    |    |   isSuccess() = <b>true</b>      |
 * |        Uncompleted       |    |    +===========================+
 * +--------------------------+    |    | Completed with failure    |
 * |      isDone() = false    |    |    +---------------------------+
 * |   isSuccess() = false    |----+---->   isDone() = <b>true</b>         |
 * | isCancelled() = false    |    |    | getCause() = <b>non-null</b>     |
 * |    getCause() = null     |    |    +===========================+
 * +--------------------------+    |    | Completed by cancellation |
 *                                 |    +---------------------------+
 *                                 +---->      isDone() = <b>true</b>      |
 *                                      | isCancelled() = <b>true</b>      |
 *                                      +---------------------------+
 * </pre>
 * 
 */
public interface WindowFuture<K,R,P> {

    /** 调用者不等待 */
    static public final int CALLER_NOT_WAITING = 0;
    /** 调用者等待 */
    static public final int CALLER_WAITING = 1;
    /** 调用者等待，但有超时限制 (timeout) */
    static public final int CALLER_WAITING_TIMEOUT = 2;

    /**
     * 得到窗口的key
     * @return The key of the window entry.
     */
    public K getKey();

    /**
     * 获取窗口发送的消息对象
     * @return The request contained in the window entry.
     */
    public R getRequest();
    
    /**
     * 获取窗口消息对应的响应对象
     * @return The response associated with he request or null if no response
     *      has been received.
     */
    public P getResponse();
    
    /**
     * 窗口发送是否完成
     * or cancelled.
     */
    public boolean isDone();

    /**
     * 窗口发送是否成功
     */
    public boolean isSuccess();
    
    /**
     * 获取失败原因
     *
     * @return the cause of the failure.
     *         {@code null} if succeeded or this future is not
     *         completed yet.
     */
    public Throwable getCause();
    
    /**
     * 是否被取消
     */
    public boolean isCancelled();

    /**
     * 获取调用者的请求要求类型
     * @return The hint of the state of the caller
     */
    public int getCallerStateHint();
    
    /**
     * 调用者是否等待
     */
    public boolean isCallerWaiting();
    
    /**
     * 获取窗口大小
     * @return The size of the window after this request was added.
     */
    public int getWindowSize();
    
    /**
     * 是否有设置超时
     * @return True if an expire timestamp exists
     */
    public boolean hasExpireTimestamp();
    
    /**
     * 获取超时时间，毫秒
     * @see #hasExpireTimestamp() 
     */
    public long getExpireTimestamp();
    
    /**
     * 获取请求句柄供时间
     * @return The offer timestamp
     */
    public long getOfferTimestamp();
    
    /**
     * 获取窗口分配句柄时间
     * @return The accept timestamp
     */
    public long getAcceptTimestamp();
    
    /**
     * 获取请求到获取句柄的时间差
     * @return The amount of time from offer to accept
     */
    public long getOfferToAcceptTime();
    
   
    /**
     * 是否有完成时间戳
     * @return True if a done timestamp exists
     */
    public boolean hasDoneTimestamp();
    
    /**
     * 获取完成时间
     * @return The done timestamp
     */
    public long getDoneTimestamp();
    
    /**
     * 获取请求到完成消息的时间
     * @return The amount of time from offer to done
     */
    public long getOfferToDoneTime();
    
    /**
     * 获取分配句柄到完成的时间
     * @return The amount of time from accept to done
     */
    public long getAcceptToDoneTime();
    
    /**
     * 响应消息，并设置完成时间
     * @param response The response for the associated request
     */
    public void complete(P response);
    
    /**
     * 响应消息，并设置指定时间
     * @param response The response for the associated request
     * @param doneTimestamp The timestamp when the request completed
     */
    public void complete(P response, long doneTimestamp);
    
    /**
     * 完成但是由于失败导致
     * @param t The throwable as the cause of failure
     */
    public void fail(Throwable t);
    
    /**
     * 设置失败的时间和状态
     * @param t The throwable as the cause of failure
     * @param doneTimestamp The timestamp when the request failed
     */
    public void fail(Throwable t, long doneTimestamp);
    
    /**
     * 取消发送句柄
     */
    public void cancel();
    
    /**
     * 取消句柄，并设置当前时间
     * @param doneTimestamp The timestamp when the request was cancelled
     */
    public void cancel(long doneTimestamp);
    
    /**
     * 等待句柄完成，并且等待指定的超时时间
     * @return True if and only if the future was completed within the specified
     *      time limit 
     * @throws InterruptedException Thrown if the current thread was interrupted
     */
    public boolean await() throws InterruptedException;
    
    /**
     * 等待句柄完成，并设置超时时间
     * @param timeoutMillis The amount of milliseconds to wait
     * @return True if and only if the future was completed within the specified
     *      time limit 
     * @throws InterruptedException Thrown if the current thread was interrupted
     */
    public boolean await(long timeoutMillis) throws InterruptedException;
}
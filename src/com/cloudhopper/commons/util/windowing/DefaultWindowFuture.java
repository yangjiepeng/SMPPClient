package com.cloudhopper.commons.util.windowing;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 默认的WindowFuture实现
 */
public class DefaultWindowFuture<K, R, P> implements WindowFuture<K, R, P> {

	private final WeakReference<Window> window; // 弱引用窗口信息
	private final ReentrantLock windowLock;
	private final Condition completedCondition; // 锁的条件对象
	private final K key;
	private final R request;
	private final AtomicReference<P> response;
	private final AtomicReference<Throwable> cause;
	private final AtomicInteger callerStateHint;
	private final AtomicBoolean done;
	private final long originalOfferTimeoutMillis;
	private final int windowSize;
	private final long offerTimestamp;
	private final long acceptTimestamp;
	private final long expireTimestamp;
	private final AtomicLong doneTimestamp;

	/**
	 * 创建WindowFuture对象
	 * 
	 * @param window 创建futrue的窗口.
	 * @param windowLock 发送窗口的锁对象
	 * @param completedCondition 等待锁条件
	 * @param key futrue的key
	 * @param request 对应的请求对象
	 * @param callerStateHint
	 * @param originalOfferTimeoutMillis
	 * @param windowSize.
	 * @param offerTimestamp
	 * @param acceptTimestamp
	 * @param expireTimestamp
	 */
	protected DefaultWindowFuture(Window window, ReentrantLock windowLock, Condition completedCondition, K key,
			R request, int callerStateHint, long originalOfferTimeoutMillis, int windowSize, long offerTimestamp,
			long acceptTimestamp, long expireTimestamp) {
		this.window = new WeakReference<Window>(window);
		this.windowLock = windowLock;
		this.completedCondition = completedCondition;
		this.key = key;
		this.request = request;
		this.response = new AtomicReference<P>();
		this.cause = new AtomicReference<Throwable>();
		this.callerStateHint = new AtomicInteger(callerStateHint);
		this.done = new AtomicBoolean(false);
		this.originalOfferTimeoutMillis = originalOfferTimeoutMillis;
		this.windowSize = windowSize;
		this.offerTimestamp = offerTimestamp;
		this.acceptTimestamp = acceptTimestamp;
		this.expireTimestamp = expireTimestamp;
		this.doneTimestamp = new AtomicLong(0);
	}

	@Override
	public K getKey() {
		return this.key;
	}

	@Override
	public R getRequest() {
		return this.request;
	}

	@Override
	public P getResponse() {
		return this.response.get();
	}

	@Override
	public Throwable getCause() {
		return this.cause.get();
	}

	@Override
	public int getCallerStateHint() {
		return this.callerStateHint.get();
	}

	public void setCallerStateHint(int callerState) {
		this.callerStateHint.set(callerState);
	}

	@Override
	public boolean isCallerWaiting() {
		return (this.callerStateHint.get() == CALLER_WAITING);
	}

	@Override
	public int getWindowSize() {
		return this.windowSize;
	}

	@Override
	public boolean hasExpireTimestamp() {
		return (this.expireTimestamp > 0);
	}

	@Override
	public long getExpireTimestamp() {
		return this.expireTimestamp;
	}

	@Override
	public long getOfferTimestamp() {
		return this.offerTimestamp;
	}

	@Override
	public long getAcceptTimestamp() {
		return this.acceptTimestamp;
	}

	@Override
	public boolean hasDoneTimestamp() {
		return (this.doneTimestamp.get() > 0);
	}

	@Override
	public long getDoneTimestamp() {
		return this.doneTimestamp.get();
	}

	@Override
	public long getOfferToAcceptTime() {
		return (this.acceptTimestamp - this.offerTimestamp);
	}

	@Override
	public long getOfferToDoneTime() {
		if (this.done.get()) {
			return (this.doneTimestamp.get() - this.offerTimestamp);
		} else {
			return -1;
		}
	}

	@Override
	public long getAcceptToDoneTime() {
		if (this.done.get()) {
			return (this.doneTimestamp.get() - this.acceptTimestamp);
		} else {
			return -1;
		}
	}

	@Override
	public boolean isDone() {
		return this.done.get();
	}

	private void lockAndSignalAll() {
		// notify any waiters that we're done
		windowLock.lock();
		try {
			completedCondition.signalAll();
		} finally {
			windowLock.unlock();
		}
	}

	@Override
	public boolean isSuccess() {
		return (this.done.get() && (this.response.get() != null));
	}

	@Override
	public void complete(P response) {
		complete(response, System.currentTimeMillis());
	}

	@Override
	public void complete(P response, long doneTimestamp) {
		completeHelper(response, doneTimestamp);
		safelyRemoveRequestInWindow();
		lockAndSignalAll();
	}

	private void safelyRemoveRequestInWindow() {
		Window window0 = this.window.get();
		if (window0 == null) {
			// hmm.. this means the window was garbage collected (uh oh)
		} else {
			window0.removeHelper(key);
		}
	}

	void completeHelper(P response, long doneTimestamp) {
		if (response == null) {
			throw new IllegalArgumentException("A response cannot be null if trying to complete()");
		}
		if (doneTimestamp <= 0) {
			throw new IllegalArgumentException("A valid doneTime must be > 0 if trying to complete()");
		}
		// set to done, but don't handle duplicate calls
		if (!this.done.get()) {
			this.response.set(response);
			this.doneTimestamp.set(doneTimestamp);
			this.done.set(true);
		}
	}

	@Override
	public void fail(Throwable t) {
		fail(t, System.currentTimeMillis());
	}

	@Override
	public void fail(Throwable t, long doneTimestamp) {
		failedHelper(t, doneTimestamp);
		safelyRemoveRequestInWindow();
		lockAndSignalAll();
	}

	void failedHelper(Throwable t, long doneTimestamp) {
		if (t == null) {
			throw new IllegalArgumentException("A response cannot be null if trying to failed()");
		}
		if (doneTimestamp <= 0) {
			throw new IllegalArgumentException("A valid doneTimestamp must be > 0 if trying to failed()");
		}
		// set to done, but don't handle duplicate calls
		if (!this.done.get()) {
			this.cause.set(t);
			this.doneTimestamp.set(doneTimestamp);
			this.done.set(true);
		}
	}

	@Override
	public boolean isCancelled() {
		return (this.done.get() && (this.response.get() == null) && (this.cause.get() == null));
	}

	@Override
	public void cancel() {
		cancel(System.currentTimeMillis());
	}

	@Override
	public void cancel(long doneTimestamp) {
		cancelHelper(doneTimestamp);
		safelyRemoveRequestInWindow();
		lockAndSignalAll();
	}

	void cancelHelper(long doneTimestamp) {
		if (doneTimestamp <= 0) {
			throw new IllegalArgumentException("A valid doneTimestamp must be > 0 if trying to cancel()");
		}
		// set to done, but don't handle duplicate calls
		if (this.done.compareAndSet(false, true)) {
			this.doneTimestamp.set(doneTimestamp);
		}
	}

	@Override
	public boolean await() throws InterruptedException {
		// offerTimeoutMillis - offerToAcceptTime
		long remainingTimeoutMillis = this.originalOfferTimeoutMillis - this.getOfferToAcceptTime();
		return this.await(remainingTimeoutMillis);

	}

	@Override
	public boolean await(long timeoutMillis) throws InterruptedException {
		// k, if someone actually calls this method -- make sure to set the flag
		this.setCallerStateHint(CALLER_WAITING);

		if (isDone()) {
			return true;
		}

		long startTime = System.currentTimeMillis();
		// 尝试等待
		if (!windowLock.tryLock(timeoutMillis, TimeUnit.MILLISECONDS)) {
			this.setCallerStateHint(CALLER_WAITING_TIMEOUT);
			return false;
		}

		try {
			// 保持等待，直到完成
			while (!isDone()) {
				// "waitTime" 是 ("now" - startTime)
				long waitingTime = System.currentTimeMillis() - startTime;
				if (waitingTime >= timeoutMillis) {
					// 设置超时状态
					this.setCallerStateHint(CALLER_WAITING_TIMEOUT);
					return false;
				}
				// c计算剩余超时时间
				long remainingWaitTime = timeoutMillis - waitingTime;
				// 等待消息
				completedCondition.await(remainingWaitTime, TimeUnit.MILLISECONDS);
			}
		} finally {
			windowLock.unlock();
		}

		return true;
	}
}
